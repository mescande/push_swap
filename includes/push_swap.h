/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 17:46:09 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/11 13:04:47 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <stdlib.h>
# include <unistd.h>
# include "libft.h"

typedef struct s_spitflap {
	int					val;
	int					true_val;
	struct s_spitflap	*next;
	struct s_spitflap	*prev;
}	t_flap;

/*
 * Main
*/
int		push_swap(int ac, char **av);

/*
 * Create spitflaps --- Parsing
*/
t_flap	*create_spitflap(int ac, char **av);

# define LEN_OF_INTMAX 10

int		verif_val(char *str);
int		verif_dup(int i, t_flap *top, t_flap *tab);

/*
 * Sorts and selection
*/
void	choose_sort(t_flap *topa, t_flap *topb, int len);

int		is_sorted(t_flap *topa);

void	move_to_hitted_spot(int dir, int val, t_flap **topa, t_flap **topb);
void	move_forward_further(int dir, int val, t_flap **topa, t_flap **topb);
void	move_the_other_side(int dir, int val, t_flap **topa, t_flap **topb);

typedef void	(*t_op)(t_flap **, t_flap **);
int		get_val(t_flap *top, int dir);
int		block_has_flap(t_flap *top, int i, int *bloc_size);
int		hitted_spot(int dir, int val, t_flap *top);
int		is_shorter_way_forward(int dir, int val, t_flap *top);

void	sort_blocks(t_flap *topa, t_flap *topb, int bloc_number);

void	turn_sorted_ascending(t_flap **top);
void	turn_sorted_descending(t_flap **top);

void	special_sort_2(t_flap *topa, t_flap *topb);
void	special_sort_3(t_flap *topa, t_flap *topb);
void	special_sort_5(t_flap *topa, t_flap *topb);
void	backtracking_powerfull_sort(t_flap *topa, t_flap *topb);

/*
 * Usefull spitflaps functions
 * and authorized moves
*/
int		spit_len(t_flap *top);

int		is_val_placed_ascending_order(int val, t_flap **top);
int		is_val_placed_descending_order(int val, t_flap **top);

void	sa(t_flap **topa, t_flap **topb);
void	pa(t_flap **topa, t_flap **topb);
void	ra(t_flap **topa, t_flap **topb);
void	rra(t_flap **topa, t_flap **topb);

void	sb(t_flap **topa, t_flap **topb);
void	pb(t_flap **topa, t_flap **topb);
void	rb(t_flap **topa, t_flap **topb);
void	rrb(t_flap **topa, t_flap **topb);

void	ss(t_flap **topa, t_flap **topb);
void	rr(t_flap **topa, t_flap **topb);
void	rrr(t_flap **topa, t_flap **topb);

/*
 * Error messages prints
*/
int		free_n_ret_int(void *to_free, int val);
t_flap	*free_n_return(t_flap *to_free, t_flap *to_ret);
t_flap	*t_flap_error_print(char *message, int val);
int		error_print(char *message, int val);
int		print_n_return(char *message, int val, int fd);

void	show_spits(t_flap *topa, t_flap *topb);

/*
 *
*/
#endif
