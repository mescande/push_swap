/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memrealloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mescande <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/04 12:59:20 by mescande          #+#    #+#             */
/*   Updated: 2021/01/15 15:42:33 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memrealloc(void *buff, size_t end, size_t init)
{
	char	*res;

	res = malloc(end);
	if (!res)
		return (NULL);
	res = ft_memcpy(res, buff, init);
	if (end - init > 0)
		ft_bzero(res + init, end - init);
	free(buff);
	return (res);
}
