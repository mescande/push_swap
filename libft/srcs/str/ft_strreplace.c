/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strreplace.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 15:54:25 by matthieu          #+#    #+#             */
/*   Updated: 2021/06/07 11:26:47 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdlib.h"

char	*ft_strreplace(char *src, char *del, char *replace)
{
	const int	init = ft_strlen(src);
	int			end;
	int			occurence;
	char		*res;

	occurence = (ft_strstr(src, del) - src);
	end = init - ft_strlen(del) + ft_strlen(replace);
	res = malloc(end);
	if (!res)
		return (res);
	res = ft_memcpy(res, src, occurence);
	ft_memcpy(res + occurence, replace, ft_strlen(replace));
	end = ft_strlen(del);
	src += occurence + end;
	ft_memcpy(res + occurence + ft_strlen(replace), src, ft_strlen(src));
	free (src - occurence - end);
	return (res);
}
