/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/27 11:30:10 by matthieu          #+#    #+#             */
/*   Updated: 2021/06/27 16:28:30 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdlib.h"

char	*ft_strnjoin(char *s1, const char *s2, size_t len)
{
	char	*res;

	if (!s1 && !s2)
		return (NULL);
	if (!s1)
		return (ft_strldup(s2, len));
	if (!s2)
		return (s1);
	res = ft_strnew(len + ft_strlen(s1));
	if (res == NULL)
		return (NULL);
	res = ft_strncat(ft_strcpy(res, s1), s2, len);
	free(s1);
	return (res);
}
