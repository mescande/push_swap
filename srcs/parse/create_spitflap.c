/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_spitflap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 16:03:30 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/10 10:22:50 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	set_val(t_flap *tab, int i, int val)
{
	if (i == 0)
	{
		tab[0].true_val = val;
		tab[0].next = tab + 0;
		tab[0].prev = tab + 0;
		return ;
	}
	tab[i].true_val = val;
	tab[i].next = tab + 0;
	tab[i].prev = tab[0].prev;
	tab[0].prev = tab + i;
	tab[i - 1].next = tab + i;
}

static int	max_spit_val(t_flap *tab, int len)
{
	int	i;
	int	max;

	i = -1;
	max = 0;
	while (++i < len)
		if (tab[i].true_val > tab[max].true_val)
			max = i;
	return (max);
}

static int	change_values(t_flap *tab, int len)
{
	int		min;
	char	*changed;
	int		i;
	int		j;
	int		max;

	max = max_spit_val(tab, len);
	changed = (char *)ft_memalloc(len * sizeof(char));
	if (!changed)
		return (1);
	i = -1;
	while (++i < len)
	{
		min = max;
		j = -1;
		if (verif_dup(tab[i].true_val, tab + i, tab[i].next))
			return (free_n_ret_int(changed, 1));
		while (++j < len)
			if (tab[j].true_val < tab[min].true_val && !changed[j])
				min = j;
		tab[min].val = i;
		changed[min] = 1;
	}
	free(changed);
	return (0);
}

t_flap	*create_spitflap(int ac, char **av)
{
	t_flap	*tab;
	int		i;
	t_flap	*top;

	if (ac == 1)
		return (t_flap_error_print("Arguments needed", 0));
	tab = (t_flap *)ft_memalloc(ac * sizeof(t_flap));
	if (!tab)
		return (t_flap_error_print("Allocation failed", 0));
	i = 1;
	if (verif_val(av[i]))
		return (free_n_return(tab, NULL));
	set_val(tab, 0, ft_atoi(av[i]));
	top = tab;
	while (++i < ac)
	{
		if (verif_val(av[i]))
			return (free_n_return(top, NULL));
		set_val(tab, i - 1, ft_atoi(av[i]));
	}
	if (change_values(tab, ac - 1))
		return (free_n_return(top, NULL));
	return (top);
}
