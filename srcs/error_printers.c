/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_printers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 18:01:53 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/11 13:05:05 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	free_n_ret_int(void *to_free, int val)
{
	free(to_free);
	return (val);
}

t_flap	*free_n_return(t_flap *to_free, t_flap *to_ret)
{
	free(to_free);
	return (to_ret);
}

int	print_n_return(char *message, int val, int fd)
{
	ft_putstr_fd(message, fd);
	if (!write(fd, "\n", 1))
		return (val);
	return (val);
}

t_flap	*t_flap_error_print(char *message, int val)
{
	error_print(message, val);
	return (NULL);
}

int	error_print(char *message, int val)
{
	ft_putstr_fd("Error :", 2);
	return (print_n_return(message, val, 2));
}
