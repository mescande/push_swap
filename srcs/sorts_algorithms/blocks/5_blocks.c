/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   5_blocks.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/22 12:13:11 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/08 16:08:29 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	find_direction(t_flap *top, int i, int *blc)
{
	int		next;
	int		prev;
	t_flap	*save;

	next = 0;
	save = top;
	while (!(blc[i - 1] <= top->val && top->val <= blc[i]))
	{
		top = top->next;
		next++;
	}
	prev = 0;
	while (!(blc[i - 1] <= save->val && save->val <= blc[i]) && prev < next)
	{
		save = save->prev;
		prev++;
	}
	if (prev == next)
		return (next);
	return (-prev);
}

void	move_dir(int dir, int val, t_flap **topa, t_flap **topb)
{
	if (hitted_spot(dir, val, *topb))
		move_to_hitted_spot(dir, val, topa, topb);
	else
	{
		if (is_shorter_way_forward(dir, val, *topb))
			move_forward_further(dir, val, topa, topb);
		else
			move_the_other_side(dir, val, topa, topb);
	}
	pb(topa, topb);
}

void	define_bloc_size(int *bloc_size, int len, int bloc_number)
{
	int		i;
	int		original;
	int		add;

	i = 1;
	original = len / bloc_number;
	len -= (original * bloc_number);
	add = 1;
	bloc_size[0] = 0;
	while (i <= bloc_number)
	{
		if (len == 0)
			add = 0;
		else
			len--;
		bloc_size[i] = bloc_size[i - 1] + original + add;
		i++;
	}
}

void	sort_blocks(t_flap *topa, t_flap *topb, int bloc_number)
{
	int		dir;
	int		bloc_size[12];
	int		i;

	define_bloc_size(bloc_size, spit_len(topa), bloc_number);
	i = 1;
	while (i <= bloc_number && !is_sorted(topa))
	{
		while (block_has_flap(topa, i, bloc_size))
		{
			dir = find_direction(topa, i, bloc_size);
			move_dir(dir, get_val(topa, dir), &topa, &topb);
		}
		i++;
	}
	turn_sorted_descending(&topb);
	while (topb)
		pa(&topa, &topb);
}
