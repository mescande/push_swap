/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_sort_3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/22 12:11:56 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 11:43:07 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	special_sort_3(t_flap *topa, t_flap *topb)
{
	if (topa->val < topa->next->val && topa->next->val < topa->prev->val)
		return ;
	if (topa->val < topa->prev->val && topa->prev->val < topa->next->val)
		sa(&topa, &topb);
	if (topa->prev->val < topa->next->val && topa->next->val < topa->val)
		ra(&topa, &topb);
	if (topa->next->val < topa->val && topa->val < topa->prev->val)
		sa(&topa, &topb);
	if (topa->prev->val < topa->val && topa->val < topa->next->val)
		rra(&topa, &topb);
	if (topa->next->val < topa->prev->val && topa->prev->val < topa->val)
		ra(&topa, &topb);
}
