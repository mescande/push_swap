/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/26 12:32:42 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/24 14:01:17 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	turn_sorted_ascending(t_flap **top)
{
	t_flap	*save;
	int		len;

	len = 0;
	save = *top;
	while (save->val > save->prev->val)
	{
		save = save->next;
		len++;
	}
	save = *top;
	while (save->val > save->prev->val)
	{
		save = save->prev;
		len--;
	}
	while (len < 0 && (*top)->val > (*top)->prev->val)
		ra(top, NULL);
	while (len >= 0 && (*top)->val > (*top)->prev->val)
		rra(top, NULL);
}

void	turn_sorted_descending(t_flap **top)
{
	t_flap	*save;
	int		len;

	len = 0;
	save = *top;
	while (save->val < save->prev->val)
	{
		save = save->next;
		len++;
	}
	save = *top;
	while (save->val < save->prev->val)
	{
		save = save->prev;
		len--;
	}
	while (len < 0 && (*top)->val < (*top)->prev->val)
		rb(NULL, top);
	while (len >= 0 && (*top)->val < (*top)->prev->val)
		rrb(NULL, top);
}
