/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_2_operations.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/01 16:43:22 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 09:27:18 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ss(t_flap **topa, t_flap **topb)
{
	(*topa)->val += (*topa)->next->val;
	(*topa)->next->val = (*topa)->val - (*topa)->next->val;
	(*topa)->val -= (*topa)->next->val;
	(*topb)->val += (*topb)->next->val;
	(*topb)->next->val = (*topb)->val - (*topb)->next->val;
	(*topb)->val -= (*topb)->next->val;
	if (write(STDOUT_FILENO, "ss\n", 3))
		return ;
}

void	rr(t_flap **topa, t_flap **topb)
{
	*topa = (*topa)->next;
	*topb = (*topb)->next;
	if (write(STDOUT_FILENO, "rr\n", 3))
		return ;
}

void	rrr(t_flap **topa, t_flap **topb)
{
	*topa = (*topa)->prev;
	*topb = (*topb)->prev;
	if (write(STDOUT_FILENO, "rrr\n", 4))
		return ;
}
