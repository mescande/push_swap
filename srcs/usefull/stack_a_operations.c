/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_a_operations.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/26 12:55:38 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 10:06:10 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	sa(t_flap **topa, t_flap **topb)
{
	(void)topb;
	(*topa)->val += (*topa)->next->val;
	(*topa)->next->val = (*topa)->val - (*topa)->next->val;
	(*topa)->val -= (*topa)->next->val;
	if (write(STDOUT_FILENO, "sa\n", 3))
		return ;
}

void	pa(t_flap **topa, t_flap **topb)
{
	t_flap	*newtopa;
	t_flap	*newtopb;

	if (!(*topb))
		return ;
	newtopa = *topb;
	newtopb = (*topb)->next;
	(*topb)->prev->next = newtopb;
	newtopb->prev = (*topb)->prev;
	if (*topb != newtopb)
		*topb = newtopb;
	else
		*topb = NULL;
	if (!(*topa))
	{
		*topa = newtopa;
		(*topa)->prev = *topa;
	}
	newtopa->next = *topa;
	newtopa->prev = (*topa)->prev;
	(*topa)->prev = newtopa;
	newtopa->prev->next = newtopa;
	*topa = newtopa;
	if (write(STDOUT_FILENO, "pa\n", 3))
		return ;
}

void	ra(t_flap **topa, t_flap **topb)
{
	(void)topb;
	*topa = (*topa)->next;
	if (write(STDOUT_FILENO, "ra\n", 3))
		return ;
}

void	rra(t_flap **topa, t_flap **topb)
{
	(void)topb;
	*topa = (*topa)->prev;
	if (write(STDOUT_FILENO, "rra\n", 4))
		return ;
}
