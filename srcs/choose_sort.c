/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   choose_sort.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/20 12:39:08 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/08 15:47:47 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	choose_sort(t_flap *topa, t_flap *topb, int len)
{
	if (is_sorted(topa))
		return ;
	if (len == 2)
		special_sort_2(topa, topb);
	else if (len == 3)
		special_sort_3(topa, topb);
	else if (len == 4 || len == 5)
		special_sort_5(topa, topb);
	else if (len < 101)
		sort_blocks(topa, topb, 5);
	else
		sort_blocks(topa, topb, 11);
}
