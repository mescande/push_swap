# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mescande <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/02 14:03:19 by mescande          #+#    #+#              #
#    Updated: 2021/10/10 10:01:32 by user42           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	push_swap

MAKE		+= --no-print-directory

CC			?=	clang-9

ifndef CFLAGS
CFLAGS		=	-Wall -Wextra -Werror
CFLAGS		+=	-g
CFLAGS		+=	-fsanitize=address
endif

LEN_NAME	=	`printf "%s" $(NAME) | wc -c`
DELTA		=	$$(echo "$$(tput cols)-32-$(LEN_NAME)" | bc)

SRC_DIR		=	srcs/
OBJ_DIR		=	objs/
INC_DIR		=	includes/
LIB_LIB		=	libft.a
LIB_DIR		=	libft/
LIB_INC		=	libft/includes/

INC_FLAGS	=	-iquote$(INC_DIR) -iquote$(LIB_INC) -L $(LIB_DIR) -l ft

SRC_LIST	=	choose_sort.c\
				error_printers.c\
				main.c\
				parse/create_spitflap.c\
				parse/verif_val.c\
				push_swap.c\
				sorts_algorithms/blocks/5_blocks.c\
				sorts_algorithms/blocks/find_flap.c\
				sorts_algorithms/blocks/moves_sides.c\
				sorts_algorithms/under5/special_sort_2.c\
				sorts_algorithms/under5/special_sort_3.c\
				sorts_algorithms/under5/special_sort_5.c\
				sorts_algorithms/under5/special_sort.c\
				usefull/is_placed_spitflap.c\
				usefull/lens.c\
				usefull/stack_2_operations.c\
				usefull/stack_a_operations.c\
				usefull/stack_b_operations.c\
				verif_sorted.c
#				usefull/show_spits.c\

SRC			=	$(addprefix $(SRC_DIR), $(SRC_LIST))
OBJ			=	$(addprefix $(OBJ_DIR), $(SRC_LIST:.c=.o))
DIR			=	$(sort $(dir $(OBJ)))
NB			=	$(words $(SRC_LIST))
INDEX		=	0

SHELL		:=	/bin/bash

all: 
	@$(MAKE) -j -C $(LIB_DIR) $(LIB_LIB)
	@$(MAKE) -j $(NAME)
	@printf "\r\033[38;5;117m✓ MAKE $@\033[0m\033[K\n"

$(NAME):		$(OBJ) Makefile $(LIB_DIR)$(LIB_LIB) 
	@$(CC) $(CFLAGS) -MMD $(OBJ) -o $@ $(INC_FLAGS)
	@printf "\r\033[38;5;117m✓ MAKE $(NAME)\033[0m\033[K\n"

$(OBJ_DIR)%.o:	$(SRC_DIR)%.c Makefile | $(DIR)
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval TO_DO=$(shell echo $$((20-$(INDEX)*20/$(NB) - 1))))
	@$(eval COLOR=$(shell list=(160 196 202 208 215 221 227 226 190 154 118 84 46); index=$$(($(PERCENT) * $${#list[@]} / 100)); echo "$${list[$$index]}"))
	@printf "\r\033[38;5;%dm↻ [%s]: %2d%% `printf '█%.0s' {0..$(DONE)}`%*s❙%s\033[0m\033[K" $(COLOR) $(NAME) $(PERCENT) $(TO_DO) "" "$(shell echo "$@" | sed 's/^.*\/\(.*\).[och]$$/\1/')"
	@$(CC) $(CFLAGS) -MMD -c $< -o $@ $(INC_FLAGS)
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))

$(DIR):
	@mkdir -p $@

clean:
	@$(MAKE) -C $(LIB_DIR) clean
	@rm -rf $(OBJ_DIR)
	@printf "\r\033[33;5;117m✓ MAKE $@\033[0m\033[K\n"

fclean: clean
	@$(MAKE) -C $(LIB_DIR) fclean
	@rm -rf $(NAME)
	@printf "\r\033[33;5;117m✓ MAKE $@\033[0m\033[K\n"

re: fclean
	@$(MAKE) all

nolib:
	@echo "Recompiling everything except libs"
	@rm -rf $(OBJ_DIR)
	@$(MAKE) -j $(NAME)

norme:
	norminette $(INC_DIR) $(SRC_DIR)
	@$(MAKE) -C $(LIB_DIR) norme

norminette: norme

test:
	@$(MAKE) all CFLAGS="-Wall -Wextra -g"
	./$(NAME) $(ARGS)

valgrind:
	@$(MAKE) all
	@valgrind ./$(NAME) $(ARGS)

help:
	@echo "all	: compiling everything that changed, linking, not relinking\n"
	@echo "clean	: destroy all objects and linking files from program and libs\n"
	@echo "fclean	: clean and destroy exec files and libs"
	@echo "test	: all and exec with validfile.rt or a file given in argument"
	@echo "re	: fclean all"
	@echo "nolib	: destroy object of programs only (not lib) then compiling again"
	@echo "norme	: execute a norme test on all code files but do no compile"
	@echo "help	: print this help"
	@echo "test	: compile, and run the program with ARGS for argument (default : $$""(ARGS) = $(ARGS)"
	@echo "valgrind	: compile and run the program with valgrind and ARGS for argument"

.PHONY: all clean fclean re

-include $(OBJ:.o=.d)
